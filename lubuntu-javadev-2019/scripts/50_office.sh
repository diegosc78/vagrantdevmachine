#!/bin/bash

#----- Fancy Messages -----#
show_error(){
    echo -e "\033[1;31m *** $@ ***\033[m" 1>&2
}
show_info(){
    echo -e "\033[1;32m *** $@ ***\033[0m"
}
show_warning(){
    echo -e "\033[1;33m *** $@ ***\033[0m"
}
show_question(){
    echo -e "\033[1;34m *** $@ ***\033[0m"
}
show_success(){
    echo -e "\033[1;35m *** $@ ***\033[0m"
}
show_header(){
    echo -e "\033[1;36m *** $@ ***\033[0m"
}
show_listitem(){
    echo -e "\033[0;37m *** $@ ***\033[0m"
}


#----- System common functions -----#
sys_sudocheck() {
    if [ $(id -u) -ne 0 ]; then
        echo -e "Command must be run as root. Try 'sudo $1'\n"
        exit 1
    fi
}

sys_wait_for_apt_lock(){
	until sudo apt-get --yes update; do echo "Waiting for apt lock..."; sleep 5; done
}

sys_full_upgrade(){
	apt-get -y update && apt-get -y upgrade && apt-get -y dist-upgrade && apt-get -y autoremove && apt-get -y update
}	

sys_cleanup(){
	apt-get -y update && apt-get -y autoremove && apt-get -y clean && apt-get -y update
}	

sys_download(){
    local url=$2
    local file=$1
    wget --progress=dot $url >/dev/null 2>&1
}

sys_install_package(){
    local packages=$*
    sudo apt-get install -y $packages >/dev/null 2>&1
}

#------ Custom functions ----------#

inst_libreoffice() {
    show_info "Installing libreoffice... "
    apt-get -y install libreoffice
}

inst_dgrmodelio() {
    show_info "Installing modelio dgr tool... "
    cd ~
    wget https://iweb.dl.sourceforge.net/project/modeliouml/4.0.0/modelio-open-source4.0_4.0.0_amd64.deb
    dpkg -i modelio-open-source4.0_4.0.0_amd64.deb
    rm modelio-open-source4.0_4.0.0_amd64.deb
}

inst_dgryed() {
    show_info "Installing yed dgr tool in /opt... "
    cd /opt
    wget https://www.yworks.com/resources/yed/demo/yEd-3.19.1.1.zip
    unzip yEd-3.19.1.1.zip
    rm yEd-3.19.1.1.zip
}

#TODO balsamiq

## MAIN
inst_libreoffice
inst_dgrmodelio
inst_dgryed


